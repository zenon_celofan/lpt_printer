################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../stm32_chest/WebServer/W5500/socket.c \
../stm32_chest/WebServer/W5500/w5500.c \
../stm32_chest/WebServer/W5500/wizchip_conf.c 

OBJS += \
./stm32_chest/WebServer/W5500/socket.o \
./stm32_chest/WebServer/W5500/w5500.o \
./stm32_chest/WebServer/W5500/wizchip_conf.o 

C_DEPS += \
./stm32_chest/WebServer/W5500/socket.d \
./stm32_chest/WebServer/W5500/w5500.d \
./stm32_chest/WebServer/W5500/wizchip_conf.d 


# Each subdirectory must supply rules for building sources it contributes
stm32_chest/WebServer/W5500/%.o: ../stm32_chest/WebServer/W5500/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DNDEBUG -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"H:\STM32ToolChain\Workspace\LPT_Printer\bluepill" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


