################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../bluepill/Can.cpp \
../bluepill/I2C.cpp \
../bluepill/IRQHandler.cpp \
../bluepill/Led.cpp \
../bluepill/NvMemory.cpp \
../bluepill/Piezo.cpp \
../bluepill/Pin.cpp \
../bluepill/Relay.cpp \
../bluepill/Rtc.cpp \
../bluepill/Serial.cpp \
../bluepill/Spi.cpp \
../bluepill/bluepill.cpp \
../bluepill/eeprom.cpp \
../bluepill/micros.cpp \
../bluepill/millis.cpp \
../bluepill/pc13led.cpp 

OBJS += \
./bluepill/Can.o \
./bluepill/I2C.o \
./bluepill/IRQHandler.o \
./bluepill/Led.o \
./bluepill/NvMemory.o \
./bluepill/Piezo.o \
./bluepill/Pin.o \
./bluepill/Relay.o \
./bluepill/Rtc.o \
./bluepill/Serial.o \
./bluepill/Spi.o \
./bluepill/bluepill.o \
./bluepill/eeprom.o \
./bluepill/micros.o \
./bluepill/millis.o \
./bluepill/pc13led.o 

CPP_DEPS += \
./bluepill/Can.d \
./bluepill/I2C.d \
./bluepill/IRQHandler.d \
./bluepill/Led.d \
./bluepill/NvMemory.d \
./bluepill/Piezo.d \
./bluepill/Pin.d \
./bluepill/Relay.d \
./bluepill/Rtc.d \
./bluepill/Serial.d \
./bluepill/Spi.d \
./bluepill/bluepill.d \
./bluepill/eeprom.d \
./bluepill/micros.d \
./bluepill/millis.d \
./bluepill/pc13led.d 


# Each subdirectory must supply rules for building sources it contributes
bluepill/%.o: ../bluepill/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DNDEBUG -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"H:\STM32\Workspace\LPT_Printer\bluepill" -I"H:\STM32\Workspace\LPT_Printer\stm32_chest\LPTPrinter" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


