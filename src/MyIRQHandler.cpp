#include <stdio.h>
#include <stdlib.h>
#include "IRQHandler.h"
#include "bluepill.h"
#include "LPTPrinter.h"
#include "micros.h"
#include "stm32f10x_usart.h"
#include "Serial.h"
#include "Led.h"



extern Serial		serial3;
extern LPTPrinter	panasonic;
extern Led 			board_led;
extern char			printer_line[82];


void USART3_IRQHandler(void) {

	USART_ClearITPendingBit(USART3, USART_IT_RXNE);

	static int n = 0;

	char c;

	c = serial3.usart_x->DR;
	printer_line[n++] = c;

	if (c == '\n') {
		printer_line[n] = 0;
		panasonic.print_line(printer_line);
		n = 0;
	}

   	//panasonic.send_byte((uint8_t) serial3.usart_x->DR);


} //USART3_IRQHandler()
