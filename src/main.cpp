#include <stdio.h>
#include <string.h>

#include "LPTPrinter.h"
#include "bluepill.h"
#include "millis.h"
#include "Led.h"
#include "Serial.h"


Led 		board_led(PC13);
Serial		serial3(USART3);
LPTPrinter	panasonic;
char		printer_line[82];
char		test_line[100];






void init_receive_interrupt(void);
void clear_printer_line();



void main() {

	millis_init();
	board_led.turn_on();
	delay(50);
	board_led.turn_off();
	serial3.send("\nPrinter Ready.\n");

	init_receive_interrupt();

    while(1) {

    }

} //main



void init_receive_interrupt(void) {

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitTypeDef NVIC_InitStructure;
	USART_ITConfig(USART3, USART_IT_IDLE, DISABLE);
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
	//NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

} //init_receive_interrupt()


void clear_printer_line() {
	for (int i = 0; i < 82; ++i) {
		printer_line[i] = 0;
	}
	panasonic.reset();
} //clear_printer_line


//#define USART_IT_PE                          ((uint16_t)0x0028)
//#define USART_IT_TXE                         ((uint16_t)0x0727)
//#define USART_IT_TC                          ((uint16_t)0x0626)
//#define USART_IT_RXNE                        ((uint16_t)0x0525)
//#define USART_IT_IDLE                        ((uint16_t)0x0424)
//#define USART_IT_LBD                         ((uint16_t)0x0846)
//#define USART_IT_CTS                         ((uint16_t)0x096A)
//#define USART_IT_ERR                         ((uint16_t)0x0060)
//#define USART_IT_ORE                         ((uint16_t)0x0360)
//#define USART_IT_NE                          ((uint16_t)0x0260)
//#define USART_IT_FE
